## Enviroment description

Xcode Version 7.3 (7D175) - iOS 9.3

Device iPhone 5S - iOS 9.3.2

## Before running

1. run `pod install`
2. open xcworkspace and change bundle identifier and team accordingly
3. (optional) change API key configuration

## Requirement analysis

### Definitions

1. User should be able to:
2. Select city
3. View a 5 day forecast for the selected city.
4. Add/Remove city from list
5. See his list of saved cities even if he closes the app and then open it again
6. See the forecast for the selected city

Assumptions
1. Since the place from which to get the cities was not specified I decided to use the Search endpoint from worldweatheronline API.
2. Using local storage since the place to persist the data was not specified.
3. Defining UI since it was not specified.
4. Only one city will be selected at a time.
5. The 5 day forecast doesn't count today, so the fetch will bring today plus 5 days.
6. When the app opens it tries to load last selected city.
7. The forecast is composed of current temperature, icon, weekday, min/max temperature for today and weekday, min/max temperature for next 5 days.

### Architecture

I decided to challenge myself and get as close as I could of Clean Architecture by [Uncle Bob](https://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html). This layer style of architecture envision to decouple the application and make it easier to maintain and expand, yet with simple concepts. Those are the two reason I pick this.

### 3rd Party Libraries

1. Cocoapods
2. AFNetworking
3. Realm
4. Mantle
5. Kiwi
6. Nocilla

### General Decisions

1. API configuration is made with code constants, it is easy to access and as they are typed we avoid misusage of values.
2. I used gitflow during development, so feature branches are merged into development and master will contain the first stable version delivered to MTT. I found this approach to solve lots of problems with versioning, mainly when working with development + maintenance teams.
3. The structure, access between classes and modeling was made following Clean Architecture definitions, still need adjustments to comply fully to the rules, but it should provide faster changes. For instance we can change Realm easier than if it was used directly, only Persistence layer should know about it's presence.
4. Transformers act like 'Adapters', except they don't translate legacy code, it just happens to adapt one model to another between layers.
5. Gateways act like 'Mediators', they are expected to make things interact without them have to know about each other, i.e. presenters using storage and network gateways.
Realm was picked for some reasons: I never used it before, apparently is faster than pure SQLite and CoreData, easy to check data with browser app, quite powerful for a full custom storage option, talks directly with Mantle for JSON mapping.
6. Mantle was a choice for it's simple but powerful mapping with JSON, I already used other alternatives and is more stable than some other options, in terms of update and support.
7. Kiwi & Nocilla were picked because I believe tests get simple and clear with them.
8. AFNetworking is a standard in iOS web request libraries. It does a pretty good job and is stable with lots of support.
9. A desirable couple which I decided to ignore was setting up Continuous Integration and some test distribution service with Bitrise and HockeyApp. For the purpose of this test my choice was to give up on these.
10. Another desirable item is Continuous Delivery, fastlane could do the job here. Is not easy to use, but I believe it would be manageable in a real project.

### Testing Approach

1. No integration tests
2. Avoid database, network and similar calls.
3. As there was new knowledge yet to be learned I didn't need to get too strict with Test first rule, so I used those reminders: keep tests running, avoid committing with non passing tests, test whenever possible, keep everything as simple as possible.
4. Try to make tests easier to understand, describe and context helps here.
Regarding App testing: try to break things both in Simulator and Device.

### Conclusions and considerations

1. I could have used categories instead of protocols for modeling some concrete+abstract representations. I.e. a specific storage method (search) could be composed with category following a given protocol instead of being available as a private method inside a concrete class. Yet they are similar approaches.
2. Using delegates with protocol instead of blocks would make testing easier. They would allow me to mock/stud/factory objects instead of getting my way around them.

This project allowed me to learn new things and to train my skills, I enjoyed my time doing this, thank you.
