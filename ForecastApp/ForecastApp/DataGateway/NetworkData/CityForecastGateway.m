//
//  CityForecastGateway.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityForecastGateway.h"
#import "WeatherAPI.h"

@implementation CityForecastGateway

static NSString* const defaultDays                           = @"6";
static NSString* const defaultMonthlyClimateAverage          = @"no";
static NSString* const defaultThreeHourIntervalWeatherForDay = @"no";
static NSString* const defaultForecastTimeInterval           = @"24";

-(void)fetchSingleWithParams:(NSDictionary *)params
               success:(void (^)(id))success
               failure:(void (^)(NSError *))failure{
    [self forecast:[params objectForKey:@"query"] success:success failure:failure];
}

-(void)forecast:(NSString *)searchQuery
     success:(void (^)(id))success
       failure:(void (^)(NSError * error))failure {
    
    CityWeatherRequestModel *requestModel = [CityWeatherRequestModel new];
    [requestModel setQuery:searchQuery];
    [requestModel setDays:defaultDays];
    [requestModel setForecastTimeInterval:defaultForecastTimeInterval];
    [requestModel setMonthlyClimateAverage:defaultMonthlyClimateAverage];
    [requestModel setThreeHourIntervalWeatherForDay:defaultThreeHourIntervalWeatherForDay];
    
    [[WeatherAPI shared] getForecastForCity:requestModel success:^(CityForecastResponseModel *responseModel) {
        success(responseModel);
    } failure:^(NSError *error) {
        failure(error);
    }];
}

@end
