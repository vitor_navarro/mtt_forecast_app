//
//  CitySearchGateway.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CitySearchGateway.h"
#import "SearchCityModel.h"
#import "TableCellModel.h"

@implementation CitySearchGateway

static NSString* const defaultMaxResults = @"50";

-(void)fetchWithParams:(NSDictionary *)params
               success:(void (^)(NSArray *))success
               failure:(void (^)(NSError *))failure{
    [self search:[params objectForKey:@"query"] finished:success failed:failure];
}

-(void)search:(NSString *)searchQuery
     finished:(void (^)(NSArray *))finished
       failed:(void (^)(NSError * error))failed {
    
    SearchCityRequestModel *requestModel = [SearchCityRequestModel new];
    [requestModel setQuery:searchQuery];
    [requestModel setMaxResults:defaultMaxResults];
    
    [[WeatherAPI shared] getCitiesWithRequestModel:requestModel success:^(SearchCityResponseModel *responseModel) {
        finished(responseModel.cities);
    } failure:^(NSError *error) {
        failed(error);
    }];
}

@end
