//
//  CitySearchGateway.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WeatherAPI.h"
#import "NetworkGateway.h"

@interface CitySearchGateway : NSObject <NetworkGateway>

-(void)search:(NSString *)searchQuery
     finished:(void (^)(NSArray *))finished
       failed:(void (^)(NSError * error))failed;

@end
