//
//  Gateway.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NetworkGateway <NSObject>

@optional
- (void) fetchWithParams:(NSDictionary *)params success:(void (^)(NSArray *))success failure:(void (^)(NSError *))failure;
- (void) fetchSingleWithParams:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure;

@end
