//
//  CityListPresenterTransformer.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityListPresenterTransformer.h"
#import "TableCellModel.h"
#import "CityEntity.h"

@implementation CityListPresenterTransformer

- (id)apply:(id)data {
    return [self transformToCommonData:data];
}

- (TableCellModel *)transformToCommonData:(CityEntity *)cityModel {
    NSString *detail = [NSString stringWithFormat:@"%@ - %@", cityModel.region, cityModel.country];
    
    TableCellModel *cellModel = [TableCellModel new];
    [cellModel setTitle:cityModel.name];
    [cellModel setDetail:detail];
    return cellModel;
}

@end
