//
//  CitySearchTransformer.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CitySearchPresenterTransformer.h"
#import "SearchCityModel.h"
#import "TableCellModel.h"

@implementation CitySearchPresenterTransformer

- (id)apply:(id)data {
    return [self transformToCommonData:data];
}

- (TableCellModel *)transformToCommonData:(SearchCityModel *)cityModel {
    NSString *detail = [NSString stringWithFormat:@"%@ - %@", cityModel.region, cityModel.country];
    
    TableCellModel *cellModel = [TableCellModel new];
    [cellModel setTitle:cityModel.city];
    [cellModel setDetail:detail];
    return cellModel;
}

@end
