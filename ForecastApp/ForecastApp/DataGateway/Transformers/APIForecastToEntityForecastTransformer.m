//
//  APIForecastToEntityForecastTransformer.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "APIForecastToEntityForecastTransformer.h"
#import "CityForecastResponseModel.h"
#import "CityForecastModel.h"
#import "CityForecastEntity.h"

@implementation APIForecastToEntityForecastTransformer

- (id)apply:(id)data {
    return [self transformAPIModelToEntity:data];
}

- (CityForecastEntity *)transformAPIModelToEntity:(CityForecastResponseModel *)forecastModel {
    NSString *temperature = [NSString stringWithFormat:@"%@°", forecastModel.current.temperature];
    
    CityForecastEntity *forecastEntity = [CityForecastEntity new];
    [forecastEntity setCurrentTemperature:temperature];
    [forecastEntity setCurrentWeatherIconUrl:forecastModel.current.weatherIconUrl];
    
    NSArray *apiForecasts = forecastModel.forecasts;
    if(apiForecasts.count > 0) {
        CityForecastModel *todayForecast = [apiForecasts firstObject];
        
        [forecastEntity setDate:todayForecast.date];
        [forecastEntity setMinTemperature:[NSString stringWithFormat:@"%@°", todayForecast.minTemperature]];
        [forecastEntity setMaxTemperature:[NSString stringWithFormat:@"%@°", todayForecast.maxTemperature]];
        
        NSMutableArray *nextDaysForecast = [NSMutableArray array];
        for(int i = 1; i < forecastModel.forecasts.count; i++) {
            CityForecastModel *apiForecastModel = [apiForecasts objectAtIndex:i];
            CityForecastEntity *nextDayForecast = [CityForecastEntity new];
            [nextDayForecast setDate:apiForecastModel.date];
            [nextDayForecast setMinTemperature:[NSString stringWithFormat:@"%@°", apiForecastModel.minTemperature]];
            [nextDayForecast setMaxTemperature:[NSString stringWithFormat:@"%@°", apiForecastModel.maxTemperature]];
            [nextDaysForecast addObject:nextDayForecast];
        }
        [forecastEntity setNextDaysForecast:nextDaysForecast];
    }    
    
    return forecastEntity;
}

@end
