//
//  CitySearchTransformer.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataTransformer.h"

@interface CitySearchPresenterTransformer : NSObject <DataTransformer>

@end
