//
//  APIForecastToEntityForecastTransformer.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataTransformer.h"

@interface APIForecastToEntityForecastTransformer : NSObject <DataTransformer>

@end
