//
//  CityForecastPresenterTransformer.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityForecastPresenterTransformer.h"
#import "ForecastCellModel.h"
#import "CityForecastEntity.h"

@implementation CityForecastPresenterTransformer

- (id) apply:(id)data {
    return [self transformEntityToCellModel:data];
}

- (ForecastCellModel *) transformEntityToCellModel:(CityForecastEntity *)forecastEntity {
    ForecastCellModel *cellModel = [ForecastCellModel new];
    [cellModel setCurrentTemperature:forecastEntity.currentTemperature];
    
    NSString *temperatureVariation = [NSString stringWithFormat:@"%@ / %@",
                                      forecastEntity.minTemperature, forecastEntity.maxTemperature];
    
    [cellModel setTemperatureVariation:temperatureVariation];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    NSDate *parsedDate = [dateFormatter dateFromString:forecastEntity.date];
    dateFormatter.dateFormat = @"EEEE";
    NSString *styledDate = [dateFormatter stringFromDate:parsedDate];
    
    [cellModel setDate:styledDate];
    
    BOOL haveValidIconUrl = forecastEntity.currentWeatherIconUrl != nil && ![forecastEntity.currentWeatherIconUrl isEqualToString:@""];
    if(haveValidIconUrl) {
        NSString *weatherIconUrl = forecastEntity.currentWeatherIconUrl;
        BOOL isInsecureUrl = [weatherIconUrl rangeOfString:@"https"].location == NSNotFound;
        if(isInsecureUrl) {
            weatherIconUrl = [weatherIconUrl stringByReplacingOccurrencesOfString:@"http" withString:@"https"];
            [cellModel setWeatherIconUrl:weatherIconUrl];
        }
    }
    
    return cellModel;
}

@end
