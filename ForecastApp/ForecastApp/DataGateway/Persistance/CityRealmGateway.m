//
//  CityRealmGateway.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityRealmGateway.h"
#import "SearchCityModel.h"
#import "CityRealm.h"
#import "CityEntity.h"
#import "RealmController.h"

@implementation CityRealmGateway

#pragma mark - Database Gateway Protocol

- (NSArray *)loadWhere:(NSString *)query {
    RLMResults *results = [CityRealm objectsWhere:query];
    NSMutableArray *cities = [NSMutableArray array];
    for(CityRealm *cityRealm in results){
        [cities addObject:[self cityEntityFromRealm:cityRealm]];
    }
    return cities;
}

- (NSArray *)loadAll {
    RLMResults *results = [CityRealm allObjects];
    NSMutableArray *cities = [NSMutableArray array];
    for(CityRealm *cityRealm in results){
        [cities addObject:[self cityEntityFromRealm:cityRealm]];
    }
    return cities;
}

- (CityEntity *)cityEntityFromRealm:(CityRealm *)cityRealm {
    return [[CityEntity alloc] initWithName:cityRealm.name
                                                       region:cityRealm.region
                                                      country:cityRealm.country
                                                     uniqueID:cityRealm.pkID];
}

- (void)saveModel:(id)model {
    [self saveCityModelInRealm:model];
}

- (void) deleteModelWithPrimaryKey:(NSString *)primaryKey finished:(void (^)(void))finished {
    [[RealmController shared] deleteCityForecastWithPK:primaryKey finished:finished];
}

- (void)updateModel:(id)model finished:(void (^)(void))finished {
    bool forecast = [model forecast];
    NSString *primaryKey = [model uniqueID];
    [[RealmController shared] updateCityForecast:forecast forPK:primaryKey finished:finished];
}

#pragma mark - Realm Interaction

- (void)saveCityModelInRealm:(SearchCityModel *)apiCityModel {
    CityRealm *cityRealm = [[CityRealm alloc] initWithAPIModel:apiCityModel];
    [[RealmController shared] save:cityRealm];
}

@end
