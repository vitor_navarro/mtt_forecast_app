//
//  DatabaseGateway.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DatabaseGateway <NSObject>

- (NSArray *) loadWhere:(NSString *)query;
- (NSArray *) loadAll;
- (void) saveModel:(id)model;
- (void) deleteModelWithPrimaryKey:(NSString *)primaryKey finished:(void (^)(void))finished;

@optional
- (void) updateModel:(id)model finished:(void (^)(void))finished;

@end
