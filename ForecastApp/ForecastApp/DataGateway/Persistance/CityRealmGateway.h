//
//  CityRealmGateway.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseGateway.h"

@interface CityRealmGateway : NSObject <DatabaseGateway>

@end
