//
//  CityForecastPresenter.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityForecastPresenter.h"
#import "CityEntity.h"

@interface CityForecastPresenter()

@property CityEntity *selectedCity;

@end

@implementation CityForecastPresenter

-(BOOL)hasData {
    return self.forecast != nil;
}

-(NSInteger) dataCount {
    NSInteger count = 0;
    if([self hasData]) {
        count = 1;
        if(self.forecast.nextDaysForecast != nil) {
            count += self.forecast.nextDaysForecast.count;
        }
        return count;
    }
    return 0;
}

-(ForecastCellModel *) dataAtIndex:(NSInteger)index {
    if([self hasData]){
        ForecastCellModel *cellModel;
        BOOL isCurrentForecast = index == 0;
        if(isCurrentForecast) {
            cellModel = [self.transformer apply:self.forecast];
            NSString *name = [NSString stringWithFormat:@"%@ - %@", self.selectedCity.region, self.selectedCity.country];
            [cellModel setName:name];
        }
        else{
            CityForecastEntity *nextDayForecast = [self.forecast.nextDaysForecast objectAtIndex:index-1];
            cellModel = [self.transformer apply:nextDayForecast];
        }
        return cellModel;
    }
    return [ForecastCellModel new];
}

- (void) loadSelectedCity:(id<DatabaseGateway>)dbGateway {
    self.selectedCity = [[dbGateway loadWhere:@"forecast == YES"] firstObject];
}

- (void) loadDataWithNetGateway:(id<NetworkGateway>)networkGateway finished:(void (^)(NSError *))finished {
    if(self.selectedCity == nil) {
        finished([NSError errorWithDomain:@"ForecastApp" code:404 userInfo:@{ @"message" : @"No city selected" }]);
        return;
    }
    
    [networkGateway fetchSingleWithParams:@{ @"query" : self.selectedCity.name } success:^(id model) {
        self.forecast = [self.apiToEntityTransformer apply:model];
        finished(nil);
    } failure:^(NSError *error) {
        finished(error);
    }];
}

- (NSString *) getTitle {
    if([self hasData]) {
        return self.selectedCity.name;
    }
    return NSLocalizedString(@"Select a city", @"Forecast default title");
}

@end
