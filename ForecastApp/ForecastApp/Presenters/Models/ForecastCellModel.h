//
//  ForecastCellModel.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ForecastCellModel : NSObject

@property NSString *name;
@property NSString *date;
@property NSString *temperatureVariation;
@property NSString *weatherIconUrl;
@property NSString *currentTemperature;

@end
