//
//  CityListPresenter.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityListPresenter.h"
#import "TableCellModel.h"
#import "CityEntity.h"

@interface CityListPresenter()

@property NSArray *data;
@property CityEntity *currentSelectedItem;

@end

@implementation CityListPresenter

-(BOOL)hasData {
    return self.data != nil && [self.data count] > 0;
}

-(NSInteger) dataCount {
    if([self hasData]) {
        return [self.data count];
    }
    return 0;
}

-(TableCellModel *) dataAtIndex:(NSInteger)index {
    if([self hasData]){
        id cityModel = [self.data objectAtIndex:index];
        if(self.transformer != nil){
            return [self.transformer apply:cityModel];
        }
        else {
            return cityModel;
        }
    }
    return [TableCellModel new];
}

-(id) originalDataAtIndex:(NSInteger)index {
    return [self.data objectAtIndex:index];
}

-(void) selectItemForForecast:(NSInteger)index
                withDBGateway:(id<DatabaseGateway>)dbGateway
                     finished:(void (^)(void))finished {
    if([self hasData]){
        
        if(self.currentSelectedItem != nil) {
            [self.currentSelectedItem setForecast:NO];
            [dbGateway updateModel:self.currentSelectedItem finished:^{}];
        }
        CityEntity *cityModel = [self originalDataAtIndex:index];
        [cityModel setForecast:YES];
        [dbGateway updateModel:cityModel finished:finished];
        
        self.currentSelectedItem = cityModel;
    }
}

- (void) loadSelectedCity:(id<DatabaseGateway>)dbGateway {
    self.currentSelectedItem = [[dbGateway loadWhere:@"forecast == YES"] firstObject];
}

- (void) loadDataWithGateway:(id<DatabaseGateway>)dbGateway {
    self.data = [dbGateway loadAll];
}

- (void) removeDataAtIndex:(NSInteger)index
             withDBGateway:(id<DatabaseGateway>)dbGateway
                  finished:(void (^)(void))finished {
    if([self hasData]){
        CityEntity *cityModel = [self originalDataAtIndex:index];
        [dbGateway deleteModelWithPrimaryKey:cityModel.uniqueID finished:^{
            [self loadSelectedCity:dbGateway];
            [self loadDataWithGateway:dbGateway];
            finished();
        }];
    }
}

@end
