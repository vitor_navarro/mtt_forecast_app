//
//  CitySearchPresenter.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CitySearchPresenter.h"

@interface CitySearchPresenter()

@property NSArray *data;

@end

@implementation CitySearchPresenter

-(BOOL)hasData {
    return self.data != nil && [self.data count] > 0;
}

-(NSInteger) dataCount {
    if([self hasData]) {
        return [self.data count];
    }
    return 0;
}

-(TableCellModel *) dataAtIndex:(NSInteger)index {
    if([self hasData]){
        id cityModel = [self.data objectAtIndex:index];
        if(self.transformer != nil){
            return [self.transformer apply:cityModel];
        }
        else {
            return cityModel;
        }
    }
    return [TableCellModel new];
}

-(id) originalDataAtIndex:(NSInteger)index {
    return [self.data objectAtIndex:index];
}

-(void) saveDataAtIndex:(NSInteger)index withDBGateway:(id<DatabaseGateway>)dbGateway {
    if([self hasData]){
        id cityModel = [self originalDataAtIndex:index];
        [dbGateway saveModel:cityModel];
    }
}

-(void)searchGateway:(id<NetworkGateway>)gateway
           withQuery:(NSString *)searchQuery
            finished:(void (^)(NSError *))finished {
    
    [gateway fetchWithParams:@{ @"query" : searchQuery }
                     success:^(NSArray *data) {
                        self.data = data;
                        finished(nil);
                     }
                     failure:^(NSError *error) {
                        finished(error);
                     }];
}

@end
