//
//  CitySearchPresenter.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkGateway.h"
#import "DatabaseGateway.h"
#import "TableCellModel.h"
#import "DataTransformer.h"

@interface CitySearchPresenter : NSObject

@property id<DataTransformer> transformer;

- (BOOL)hasData;
- (NSInteger) dataCount;
- (TableCellModel *) dataAtIndex:(NSInteger)index;
- (id) originalDataAtIndex:(NSInteger)index;
- (void)searchGateway:(id<NetworkGateway>)gateway
           withQuery:(NSString *)searchQuery
            finished:(void (^)(NSError *))finished;
- (void) saveDataAtIndex:(NSInteger)index withDBGateway:(id<DatabaseGateway>)dbGateway;

@end
