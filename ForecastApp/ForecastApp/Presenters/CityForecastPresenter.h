//
//  CityForecastPresenter.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataTransformer.h"
#import "DatabaseGateway.h"
#import "NetworkGateway.h"
#import "ForecastCellModel.h"
#import "CityForecastEntity.h"

@interface CityForecastPresenter : NSObject

@property CityForecastEntity *forecast;
@property id<DataTransformer> transformer;
@property id<DataTransformer> apiToEntityTransformer;

- (BOOL) hasData;
- (NSInteger) dataCount;
- (ForecastCellModel *) dataAtIndex:(NSInteger)index;
- (void) loadSelectedCity:(id<DatabaseGateway>)dbGateway;
- (void) loadDataWithNetGateway:(id<NetworkGateway>)networkGateway finished:(void (^)(NSError *))finished;
- (NSString *) getTitle;


@end
