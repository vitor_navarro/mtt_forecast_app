//
//  CityListPresenter.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseGateway.h"
#import "TableCellModel.h"
#import "DataTransformer.h"

@interface CityListPresenter : NSObject

@property id<DataTransformer> transformer;

- (BOOL) hasData;
- (NSInteger) dataCount;
- (TableCellModel *) dataAtIndex:(NSInteger)index;
- (id) originalDataAtIndex:(NSInteger)index;
- (void) loadSelectedCity:(id<DatabaseGateway>)dbGateway;
- (void) loadDataWithGateway:(id<DatabaseGateway>)dbGateway;

-(void) selectItemForForecast:(NSInteger)index
                withDBGateway:(id<DatabaseGateway>)dbGateway
                     finished:(void (^)(void))finished;
- (void) removeDataAtIndex:(NSInteger)index
             withDBGateway:(id<DatabaseGateway>)dbGateway
                  finished:(void (^)(void))finished;

@end
