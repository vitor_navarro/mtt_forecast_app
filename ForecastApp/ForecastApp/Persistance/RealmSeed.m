//
//  RealmSeed.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "RealmSeed.h"
#import <Realm/Realm.h>
#import "CityRealm.h"

@implementation RealmSeed

+(CityRealm *)newCityWithName:(NSString *)name
                       region:(NSString *)region
                      country:(NSString *)country {
    return [[CityRealm alloc] initWithDictionary:@{ @"city":name,
                                                    @"region":region,
                                                    @"country":country }];
}

+(void)createCitiesIfNotExistent {
    bool hasCities = [[CityRealm allObjects] count];
    
    if(hasCities) return;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        @autoreleasepool {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [realm addObject:[RealmSeed newCityWithName:@"London" region:@"City of London, Greater London" country:@"United Kingdom"]];
            [realm addObject:[RealmSeed newCityWithName:@"Sao Paulo" region:@"Sao Paulo" country:@"Brazil"]];
            [realm addObject:[RealmSeed newCityWithName:@"Dublin" region:@"Dublin" country:@"Ireland"]];
            [realm addObject:[RealmSeed newCityWithName:@"New York" region:@"New York" country:@"United States of America"]];
            [realm commitWriteTransaction];
        }
    });
}

@end
