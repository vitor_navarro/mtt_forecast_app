//
//  RealmController.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface RealmController : NSObject

+ (instancetype)shared;

- (void) save:(RLMObject *)realmObject;
- (void) deleteCityForecastWithPK:(NSString *)primaryKey finished:(void(^)(void))finished;
- (void) updateCityForecast:(BOOL)forecast forPK:(NSString *)primaryKeyValue finished:(void(^)(void))finished;

@end
