//
//  CityRealm.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Realm/Realm.h>
#import "BaseRealm.h"

@interface CityRealm : RLMObject <BaseRealm>

@property NSString *name;
@property NSString *region;
@property NSString *country;
@property bool forecast;
@property NSString *pkID;

@end
