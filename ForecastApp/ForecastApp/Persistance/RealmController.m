//
//  RealmController.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "RealmController.h"
#import "CityRealm.h"

@interface RealmController()

@property dispatch_queue_t realmQueue;

@end

@implementation RealmController

-(id)init{
    self = [super init];
    if(!self) return nil;
    
    self.realmQueue = dispatch_queue_create("realm", DISPATCH_QUEUE_SERIAL);
    
    return self;
}

+ (instancetype)shared {
    static id _realmController = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _realmController = [[self alloc] init];
    });
    
    return _realmController;
}

#pragma mark Realm interaction

- (void) updateCityForecast:(BOOL)forecast forPK:(NSString *)primaryKeyValue finished:(void (^)(void))finished {
    dispatch_async(self.realmQueue, ^{
        @autoreleasepool {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            CityRealm *cityRealm = [CityRealm objectForPrimaryKey:primaryKeyValue];
            [cityRealm setForecast:forecast];
            [realm addOrUpdateObject:cityRealm];
            [realm commitWriteTransaction];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                finished();
            });
        }
    });
}

- (void) deleteCityForecastWithPK:(NSString *)primaryKey finished:(void(^)(void))finished {
    dispatch_async(self.realmQueue, ^{
        @autoreleasepool {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            CityRealm *toDelete = [CityRealm objectForPrimaryKey:primaryKey];
            [realm deleteObject:toDelete];
            [realm commitWriteTransaction];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                finished();
            });
        }
    });
}


- (void) save:(RLMObject *)realmObject {
    dispatch_async(self.realmQueue, ^{
        @autoreleasepool {
            RLMRealm *realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];            
            [realm addOrUpdateObject:realmObject];
            [realm commitWriteTransaction];
        }
    });
}

@end
