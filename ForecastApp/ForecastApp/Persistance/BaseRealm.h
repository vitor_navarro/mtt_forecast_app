//
//  BaseRealm.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@protocol BaseRealm <NSObject>

@optional
- (id)initWithDictionary:(NSDictionary *)model;
- (id)initWithAPIModel:(MTLModel *)model;

@end
