//
//  CityRealm.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityRealm.h"
#import "SearchCityModel.h"

@implementation CityRealm

- (id)initWithDictionary:(NSDictionary *)model {
    self = [super init];
    if(!self) return nil;
    
    self.name    = [model objectForKey:@"city"];
    self.region  = [model objectForKey:@"region"];
    self.country = [model objectForKey:@"country"];
    self.pkID    = [self buildPrimaryKey];
    
    return self;
}

-(id)initWithAPIModel:(MTLModel *)model {
    self = [super init];
    if(!self) return nil;
    
    self.name    = [((SearchCityModel *)model) city];
    self.region  = [((SearchCityModel *)model) region];
    self.country = [((SearchCityModel *)model) country];
    self.pkID    = [self buildPrimaryKey];
    
    return self;
}

-(NSString *)buildPrimaryKey {
    NSString *cleanCity     = [self.name stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *cleanRegion   = [self.region stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *cleanCountry  = [self.country stringByReplacingOccurrencesOfString:@" " withString:@""];
    return [NSString stringWithFormat:@"%@%@%@", cleanCity, cleanRegion, cleanCountry];
}

+ (NSString *)primaryKey {
    return @"pkID";
}

@end
