//
//  ForecastTableViewCell.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForecastTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *temperatureDetail;
@property (weak, nonatomic) IBOutlet UILabel *currentTemperature;

- (void) setTitleWithValue:(NSString *)title;
- (void) setTemperatureDetailWithVariation:(NSString *)variation;
- (void) setCurrentTemperatureWithValue:(NSString *)currentTemp;
- (void) hideCurrentTemperature;
- (void) loadImageWithUrl:(NSString *)url;
- (void) setDateWithValue:(NSString *)date;

@end
