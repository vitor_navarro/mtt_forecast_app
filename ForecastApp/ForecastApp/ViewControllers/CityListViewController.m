//
//  CityListViewController.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/14/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityListViewController.h"
#import "CityListPresenter.h"
#import "CityRealmGateway.h"
#import "CityListPresenterTransformer.h"


@interface CityListViewController()

@property CityListPresenter *presenter;
@property CityRealmGateway  *databaseGateway;

@end

@implementation CityListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.presenter = [CityListPresenter new];
    [self.presenter setTransformer:[CityListPresenterTransformer new]];
    self.databaseGateway = [CityRealmGateway new];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.presenter loadSelectedCity:self.databaseGateway];
    [self.presenter loadDataWithGateway:self.databaseGateway];
    [self.tableView reloadData];
}

#pragma mark UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter dataCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellid" forIndexPath:indexPath];
    
    TableCellModel *model = [self.presenter dataAtIndex:indexPath.row];
    
    [cell.textLabel setText:model.title];
    [cell.detailTextLabel setText:model.detail];
    
    return cell;
}

#pragma mark UITableView Delegate
-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Remove" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                     {
                                         [self.presenter removeDataAtIndex:indexPath.row withDBGateway:self.databaseGateway finished:^{
                                             [tableView beginUpdates];
                                             [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                             [tableView endUpdates];
                                         }];
                                         
                                     }];
    button.backgroundColor = [UIColor redColor];
    
    return @[button];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.presenter selectItemForForecast:indexPath.row withDBGateway:self.databaseGateway finished:^{
    [self.navigationController popViewControllerAnimated:YES];
    }];
}

@end
