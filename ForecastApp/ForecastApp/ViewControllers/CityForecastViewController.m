//
//  CityForecastViewController.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/14/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityForecastViewController.h"
#import "CityForecastPresenter.h"
#import "CityRealmGateway.h"
#import "CityForecastGateway.h"
#import "CityForecastPresenterTransformer.h"
#import "ForecastTableViewCell.h"
#import "APIForecastToEntityForecastTransformer.h"


@interface CityForecastViewController()

@property CityForecastPresenter *presenter;

@end

@implementation CityForecastViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self
                            action:@selector(refreshData)
                  forControlEvents:UIControlEventValueChanged];
    
    [self.refreshControl beginRefreshing];
    [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentOffset.y-self.refreshControl.frame.size.height) animated:YES];
    
    self.presenter = [CityForecastPresenter new];
    [self.presenter setTransformer:[CityForecastPresenterTransformer new]];
    [self.presenter setApiToEntityTransformer:[APIForecastToEntityForecastTransformer new]];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.presenter loadSelectedCity:[CityRealmGateway new]];
    [self.presenter loadDataWithNetGateway:[CityForecastGateway new] finished:^(NSError *error) {
        [self setTitle:[self.presenter getTitle]];
        if(!error) {
            [self.tableView reloadData];
        }
        [self.refreshControl endRefreshing];
    }];
    [self setTitle:[self.presenter getTitle]];
}

- (void)refreshData {
    [self.presenter loadDataWithNetGateway:[CityForecastGateway new] finished:^(NSError *error) {
        [self setTitle:[self.presenter getTitle]];
        if(!error) {
            [self.tableView reloadData];
            [self.refreshControl endRefreshing];
        }
    }];
}

#pragma mark UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter dataCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ForecastTableViewCell *cell = (ForecastTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellid" forIndexPath:indexPath];
    
    ForecastCellModel *model = [self.presenter dataAtIndex:indexPath.row];
    
    [cell setCurrentTemperatureWithValue:model.currentTemperature];
    [cell setTemperatureDetailWithVariation:model.temperatureVariation];
    [cell setDateWithValue:model.date];
    [cell setTitleWithValue:model.name];
    [cell loadImageWithUrl:model.weatherIconUrl];
    
    return cell;
}

@end
