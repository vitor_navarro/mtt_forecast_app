//
//  CitySearchViewController.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/14/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CitySearchViewController.h"
#import "CitySearchPresenter.h"
#import "CitySearchGateway.h"
#import "CitySearchPresenterTransformer.h"
#import "CityRealmGateway.h"

#import "CityRealm.h"

@interface CitySearchViewController()

@property IBOutlet UITableView *tableView;

@property CitySearchPresenter *presenter;
@property id<DatabaseGateway> databaseGateway;

@end

@implementation CitySearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setTitle:NSLocalizedString(@"Add cities", @"Search default title")];
    
    self.presenter = [CitySearchPresenter new];
    [self.presenter setTransformer:[CitySearchPresenterTransformer new]];
    
    self.databaseGateway = [CityRealmGateway new];
}

#pragma mark UITableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.presenter dataCount];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellid" forIndexPath:indexPath];
    
    TableCellModel *model = [self.presenter dataAtIndex:indexPath.row];
    
    [cell.textLabel setText:model.title];
    [cell.detailTextLabel setText:model.detail];
    
    return cell;
}

#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.presenter saveDataAtIndex:indexPath.row withDBGateway:self.databaseGateway];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Success", @"Alert Success Title") message:NSLocalizedString(@"City added successfuly", @"City added success") preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action){}];

    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark SearchBar Delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.presenter searchGateway:[CitySearchGateway new]
                        withQuery:searchBar.text
                         finished:^(NSError *error) {
                            if(error == nil) {
                                [self.tableView reloadData];
                            }
                            else {
                                //TODO: present some error
                            }
                        }];
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
}

@end
