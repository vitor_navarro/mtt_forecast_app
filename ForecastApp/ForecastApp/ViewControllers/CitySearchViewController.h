//
//  CitySearchViewController.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/14/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CitySearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>

@end
