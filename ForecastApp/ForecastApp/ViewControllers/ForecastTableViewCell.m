//
//  ForecastTableViewCell.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "ForecastTableViewCell.h"
#import "UIImageView+AFNetworking.h"

@implementation ForecastTableViewCell

- (void) setTitleWithValue:(NSString *)title {
    [self.title setText:title];
}

- (void) setTemperatureDetailWithVariation:(NSString *)variation {
    [self.temperatureDetail setText:variation];
}

- (void) setCurrentTemperatureWithValue:(NSString *)currentTemp {
    [self.currentTemperature setText:currentTemp];
}

- (void) hideCurrentTemperature {
    [self.currentTemperature setHidden:YES];
}

- (void) loadImageWithUrl:(NSString *)url {
    [self.icon setImageWithURL:[NSURL URLWithString:url]];
}

- (void) setDateWithValue:(NSString *)date {
    [self.date setText:date];
}

@end
