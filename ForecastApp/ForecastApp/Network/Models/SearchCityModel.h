//
//  SearchCityModel.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface SearchCityModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *region;

+ (NSDictionary *)JSONKeyPathsByPropertyKey;

@end
