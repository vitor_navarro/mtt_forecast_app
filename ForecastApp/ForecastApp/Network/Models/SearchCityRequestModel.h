//
//  SearchCityRequestModel.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface SearchCityRequestModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *query;
@property (nonatomic, copy) NSString *maxResults;

+ (NSDictionary *)JSONKeyPathsByPropertyKey;

@end
