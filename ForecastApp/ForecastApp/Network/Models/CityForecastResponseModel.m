//
//  CityForecastResponseModel.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityForecastResponseModel.h"
#import "CityForecastModel.h"
#import "CityCurrentWeatherModel.h"

@implementation CityForecastResponseModel

#pragma mark - Mantle JSON convert

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"current"   : @"data.current_condition",
             @"forecasts" : @"data.weather"
             };
}

#pragma mark - JSON Transformer

+ (NSValueTransformer *)currentJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray *currentWeatherArray, BOOL *success,
                                                                 NSError *__autoreleasing *error) {
        NSDictionary *currentWeatherDict = [currentWeatherArray firstObject];
        BOOL hasNoValue = currentWeatherDict == nil || [currentWeatherDict count] == 0;
        if(hasNoValue) return [CityCurrentWeatherModel new];
        return [MTLJSONAdapter modelOfClass:[CityCurrentWeatherModel class] fromJSONDictionary:currentWeatherDict error:nil];
    } reverseBlock:^id(CityCurrentWeatherModel *currentWeatherModel, BOOL *success, NSError *__autoreleasing *error) {
        return @[ [MTLJSONAdapter JSONDictionaryFromModel:currentWeatherModel error:error] ];
    }];
}

+ (NSValueTransformer *)forecastsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:CityForecastModel.class];
}

@end
