//
//  CityCurrentWeatherModel.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityCurrentWeatherModel.h"

@implementation CityCurrentWeatherModel

#pragma mark - Mantle JSON convert

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"temperature": @"temp_C",
             @"temperatureInFahrenheit": @"temp_F",
             @"weatherIconUrl": @"weatherIconUrl"
             };
}

#pragma mark - JSON Transformers

+ (NSValueTransformer *)weatherIconUrlJSONTransformer {
    return [CityCurrentWeatherModel arrayJSONTransformer];
}

+ (NSValueTransformer *)arrayJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray *array, BOOL *success,
                                                                 NSError *__autoreleasing *error) {
        NSDictionary *values = array.firstObject;
        BOOL hasNoValue = values == nil || [values count] == 0;
        if(hasNoValue) return @"";
        return [values objectForKey:@"value"];
    } reverseBlock:^id(NSString *property, BOOL *success, NSError *__autoreleasing *error) {
        if(property == nil) property = @"";
        return @[ @{ @"value" : property } ];
    }];
}

@end
