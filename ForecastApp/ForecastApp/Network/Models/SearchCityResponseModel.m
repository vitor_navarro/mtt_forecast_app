//
//  SearchCityResponseModel.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "SearchCityResponseModel.h"
#import "SearchCityModel.h"

@implementation SearchCityResponseModel

#pragma mark - Mantle JSON convert

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{ @"cities" : @"search_api.result" };
}

#pragma mark - JSON Transformer

+ (NSValueTransformer *)citiesJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:SearchCityModel.class];
}


@end
