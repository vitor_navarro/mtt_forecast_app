//
//  CityForecastResponseModel.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "CityCurrentWeatherModel.h"

@interface CityForecastResponseModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) CityCurrentWeatherModel *current;
@property (nonatomic, copy) NSArray *forecasts;

+ (NSDictionary *)JSONKeyPathsByPropertyKey;

@end
