//
//  SearchCityModel.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "SearchCityModel.h"

@implementation SearchCityModel

#pragma mark - Mantle JSON convert

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"city": @"areaName",
             @"country": @"country",
             @"region": @"region"
             };
}

#pragma mark - JSON Transformers
/* The following transformers are necessary given the Weather API presents a city, country and
 region name as arrays with a model inside instead of a single string value.
 */

+ (NSValueTransformer *)cityJSONTransformer {
    return [SearchCityModel arrayJSONTransformer];
}

+ (NSValueTransformer *)countryJSONTransformer {
    return [SearchCityModel arrayJSONTransformer];
}

+ (NSValueTransformer *)regionJSONTransformer {
    return [SearchCityModel arrayJSONTransformer];
}

+ (NSValueTransformer *)arrayJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray *array, BOOL *success,
                                                                 NSError *__autoreleasing *error) {
        NSDictionary *values = array.firstObject;
        BOOL hasNoValue = values == nil || [values count] == 0;
        if(hasNoValue) return @"";
        return [values objectForKey:@"value"];
    } reverseBlock:^id(NSString *property, BOOL *success, NSError *__autoreleasing *error) {
        if(property == nil) property = @"";
        return @[ @{ @"value" : property } ];
    }];
}

@end
