//
//  CityForecastModel.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityForecastModel.h"

@implementation CityForecastModel

#pragma mark - Mantle JSON convert

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"date": @"date",
             @"maxTemperature": @"maxtempC",
             @"minTemperature": @"mintempC",
             @"maxTemperatureFahrenheit": @"maxtempF",
             @"minTemperatureFahrenheit": @"mintempF"
             };
}

@end
