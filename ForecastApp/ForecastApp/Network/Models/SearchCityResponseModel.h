//
//  SearchCityResponseModel.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface SearchCityResponseModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSArray *cities;

+ (NSDictionary *)JSONKeyPathsByPropertyKey;

@end
