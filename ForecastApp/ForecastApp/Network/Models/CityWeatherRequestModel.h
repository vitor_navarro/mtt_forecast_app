//
//  CityWeatherRequestModel.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface CityWeatherRequestModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *query;
@property (nonatomic, copy) NSString *days;
@property (nonatomic, copy) NSString *monthlyClimateAverage;
@property (nonatomic, copy) NSString *threeHourIntervalWeatherForDay;
@property (nonatomic, copy) NSString *forecastTimeInterval;

+ (NSDictionary *)JSONKeyPathsByPropertyKey;

@end
