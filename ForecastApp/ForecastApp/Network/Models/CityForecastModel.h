//
//  CityForecastModel.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface CityForecastModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *date;
@property (nonatomic, copy) NSString *minTemperature;
@property (nonatomic, copy) NSString *maxTemperature;
@property (nonatomic, copy) NSString *minTemperatureFahrenheit;
@property (nonatomic, copy) NSString *maxTemperatureFahrenheit;

+ (NSDictionary *)JSONKeyPathsByPropertyKey;



@end
