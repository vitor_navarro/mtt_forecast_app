//
//  SearchCityRequestModel.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "SearchCityRequestModel.h"

@implementation SearchCityRequestModel

#pragma mark - Mantle JSON convert

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"query": @"q",
             @"maxResults": @"num_of_results"
             };
}

@end
