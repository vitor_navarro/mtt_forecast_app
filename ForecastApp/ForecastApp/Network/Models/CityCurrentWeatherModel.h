//
//  CityCurrentWeatherModel.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface CityCurrentWeatherModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *temperature;
@property (nonatomic, copy) NSString *temperatureInFahrenheit;
@property (nonatomic, copy) NSString *weatherIconUrl;

+ (NSDictionary *)JSONKeyPathsByPropertyKey;

@end
