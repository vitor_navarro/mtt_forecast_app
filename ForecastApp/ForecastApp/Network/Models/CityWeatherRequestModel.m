//
//  CityWeatherRequestModel.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityWeatherRequestModel.h"

@implementation CityWeatherRequestModel

#pragma mark - Mantle JSON convert

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"query": @"q",
             @"days": @"num_of_days",
             @"monthlyClimateAverage": @"mca",
             @"threeHourIntervalWeatherForDay": @"fx24",
             @"forecastTimeInterval": @"tp"
             };
}

@end
