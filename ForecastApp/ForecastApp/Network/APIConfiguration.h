//
//  APIConfiguration.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const APIConfigBaseUrl;

#pragma mark Main parameters
extern NSString* const APIConfigKey;
extern NSString* const APIConfigKeyName;
extern NSString* const APIConfigFormat;
extern NSString* const APIConfigFormatName;

#pragma mark Endpoints
extern NSString* const APIConfigSearchEndpoint;
extern NSString* const APIConfigLocalWeatherEndpoint;