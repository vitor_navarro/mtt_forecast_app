//
//  WeatherAPI.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "WeatherAPI.h"
#import "APIConfiguration.h"

@implementation WeatherAPI

+ (instancetype)shared {
    return [WeatherAPI shared:APIConfigBaseUrl];
}

- (NSURLSessionDataTask *) getCitiesWithRequestModel:(SearchCityRequestModel *)requestModel
                                             success:(void (^)(SearchCityResponseModel *responseModel))success
                                             failure:(void (^)(NSError *error))failure {
    
    NSDictionary *parameters = [MTLJSONAdapter JSONDictionaryFromModel:requestModel error:nil];
    NSMutableDictionary *parametersWithKeyAndFormat = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    [parametersWithKeyAndFormat setObject:APIConfigKey forKey:APIConfigKeyName];
    [parametersWithKeyAndFormat setObject:APIConfigFormat forKey:APIConfigFormatName];
    
    
    return [self GET:APIConfigSearchEndpoint parameters:parametersWithKeyAndFormat progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSDictionary *responseDictionary = (NSDictionary *)responseObject;
                
                NSError *error;
                SearchCityResponseModel *list = [MTLJSONAdapter modelOfClass:SearchCityResponseModel.class
                                                          fromJSONDictionary:responseDictionary error:&error];
                 if(error != nil) {
                     failure(error);
                 }
                 else {
                     success(list);
                 }
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure(error);
             }];
}

- (NSURLSessionDataTask *) getForecastForCity:(CityWeatherRequestModel *)requestModel
                                             success:(void (^)(CityForecastResponseModel *responseModel))success
                                             failure:(void (^)(NSError *error))failure {
    
    NSDictionary *parameters = [MTLJSONAdapter JSONDictionaryFromModel:requestModel error:nil];
    NSMutableDictionary *parametersWithKeyAndFormat = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    [parametersWithKeyAndFormat setObject:APIConfigKey forKey:APIConfigKeyName];
    [parametersWithKeyAndFormat setObject:APIConfigFormat forKey:APIConfigFormatName];
    
    
    return [self GET:APIConfigLocalWeatherEndpoint parameters:parametersWithKeyAndFormat progress:nil
             success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                 NSDictionary *responseDictionary = (NSDictionary *)responseObject;
                 
                 NSError *error;
                 CityForecastResponseModel *list = [MTLJSONAdapter modelOfClass:[CityForecastResponseModel class]
                                                           fromJSONDictionary:responseDictionary error:&error];
                 if(error != nil) {
                     failure(error);
                 }
                 else {
                     success(list);
                 }
             }
             failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                 failure(error);
             }];
}



@end
