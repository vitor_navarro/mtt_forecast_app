//
//  APIConfiguration.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "APIConfiguration.h"

NSString* const APIConfigBaseUrl = @"https://api.worldweatheronline.com";

#pragma mark Main parameters
NSString* const APIConfigKey = @"580f9005fa694bda942201539161407";
NSString* const APIConfigKeyName = @"key";
NSString* const APIConfigFormat = @"json";
NSString* const APIConfigFormatName = @"format";

#pragma mark Endpoints
NSString* const APIConfigSearchEndpoint = @"/premium/v1/search.ashx";
NSString* const APIConfigLocalWeatherEndpoint = @"/premium/v1/weather.ashx";