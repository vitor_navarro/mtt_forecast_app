//
//  WeatherAPI.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "SessionManager.h"
#import "SearchCityRequestModel.h"
#import "SearchCityResponseModel.h"
#import "CityWeatherRequestModel.h"
#import "CityForecastResponseModel.h"

@interface WeatherAPI : SessionManager

+ (instancetype)shared;

- (NSURLSessionDataTask *) getCitiesWithRequestModel:(SearchCityRequestModel *)requestModel
                                             success:(void (^)(SearchCityResponseModel *responseModel))success
                                             failure:(void (^)(NSError *error))failure;

- (NSURLSessionDataTask *) getForecastForCity:(CityWeatherRequestModel *)requestModel
                                      success:(void (^)(CityForecastResponseModel *responseModel))success
                                      failure:(void (^)(NSError *error))failure;

@end
