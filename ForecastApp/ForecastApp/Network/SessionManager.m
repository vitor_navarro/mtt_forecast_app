//
//  SessionManager.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "SessionManager.h"
#import "APIConfiguration.h"

@implementation SessionManager

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if(!self) return nil;
    
    self.responseSerializer = [AFJSONResponseSerializer serializer];
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    
    return self;
}

+ (instancetype)shared:(NSString *)baseUrl {
    static id _sessionManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURL *url = [NSURL URLWithString:baseUrl];
        _sessionManager = [[self alloc] initWithBaseURL:url];
    });
    
    return _sessionManager;
}

@end
