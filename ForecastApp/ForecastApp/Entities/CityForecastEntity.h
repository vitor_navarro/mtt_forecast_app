//
//  CityForecastEntity.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityForecastEntity : NSObject

@property NSString *currentTemperature;
@property NSString *currentWeatherIconUrl;
@property NSString *date;
@property NSString *minTemperature;
@property NSString *maxTemperature;
@property NSArray<CityForecastEntity *> *nextDaysForecast;

@end
