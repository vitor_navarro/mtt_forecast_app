//
//  CityEntity.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityEntity : NSObject

@property NSString *name;
@property NSString *region;
@property NSString *country;
@property bool forecast;
@property NSString *uniqueID;

-(id)initWithName:(NSString *)name
   region:(NSString *)region
  country:(NSString *)country
uniqueID:(NSString *)uniqueID;

@end
