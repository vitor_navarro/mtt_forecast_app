//
//  CityEntity.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityEntity.h"

@implementation CityEntity

-(id)initWithName:(NSString *)name
                       region:(NSString *)region
                      country:(NSString *)country
                   uniqueID:(NSString *)uniqueID {
    self = [super init];
    if(!self) return nil;
    
    [self setName:name];
    [self setRegion:region];
    [self setCountry:country];
    [self setUniqueID:uniqueID];
    
    return self;
}

@end
