//
//  AppDelegate.h
//  ForecastApp
//
//  Created by Vitor Navarro on 7/14/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

