//
//  SampleNetworkGateway.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "SampleNetworkGateway.h"
#import "CityForecastResponseModel.h"

@implementation SampleNetworkGateway

- (void) fetchSingleWithParams:(NSDictionary *)params success:(void (^)(id))success failure:(void (^)(NSError *))failure {
    NSDictionary *fakeResponse = @{
      @"data" : @{ @"current_condition" :
                       @[ @{ @"temp_C" : @"19", @"temp_F" : @"19",
                             @"weatherIconUrl": @[ @{ @"value": @"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png" } ] } ],
                   @"weather": @[
                           @{
                               @"date": @"2016-07-17",
                               @"maxtempC": @"23",
                               @"maxtempF": @"73",
                               @"mintempC": @"8",
                               @"mintempF": @"46",
                               },
                           @{
                               @"date": @"2016-07-18",
                               @"maxtempC": @"20",
                               @"maxtempF": @"68",
                               @"mintempC": @"6",
                               @"mintempF": @"42",
                               }
                           ]
                   }};

    NSError *error = nil;
    CityForecastResponseModel *fakeResponseModel = [MTLJSONAdapter modelOfClass:CityForecastResponseModel.class fromJSONDictionary:fakeResponse error:&error];
    success(fakeResponseModel);
}

@end
