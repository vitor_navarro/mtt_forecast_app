//
//  CityListPresenterSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CityListPresenter.h"
#import "CityEntityDBGateway.h"

SPEC_BEGIN(CityListPresenterSpec)

describe(@"CityListPresenter", ^{
    
    let(dbGateway, ^id{
        return [CityEntityDBGateway new];
    });
    
    context(@"On initialization", ^{
        let(presenter, ^{
            return [CityListPresenter new];
        });
        
        context(@"no data is present", ^{
            it(@"count is 0", ^{
                [[theValue([presenter dataCount]) should] equal:theValue(0)];
            });
            
            it(@"data should return new model", ^{
                [[[presenter dataAtIndex:0] shouldNot] beNil];
            });
        });
    });
    
    context(@"when loading data", ^{
        let(presenter, ^{
            return [CityListPresenter new];
        });
        
        it(@"call db gateway proper method", ^{
            [[dbGateway should] receive:@selector(loadAll)];
            [presenter loadDataWithGateway:dbGateway];
        });
        
        it(@"count is 1", ^{
            [presenter loadDataWithGateway:dbGateway];
            [[theValue([presenter dataCount]) should] equal:theValue(1)];
        });
    });
    
    context(@"when selecting city", ^{
        let(presenter, ^{
            return [CityListPresenter new];
        });
        
        context(@"with previous selected city", ^{
            it(@"call update twice", ^{
                [presenter loadDataWithGateway:dbGateway];
                [presenter loadSelectedCity:dbGateway];
                
                [[dbGateway shouldEventually] receive:@selector(updateModel:finished:) withCount:2];
                [presenter selectItemForForecast:0 withDBGateway:dbGateway finished:^{}];
            });
        });
        
        
        context(@"without previous selected city", ^{
            it(@"call update once", ^{
                [presenter loadDataWithGateway:dbGateway];
                
                [[dbGateway should] receive:@selector(updateModel:finished:) withCount:1];
                [presenter selectItemForForecast:0 withDBGateway:dbGateway finished:^{}];
            });
        });
    });
    
    context(@"when removing city", ^{
        let(presenter, ^{
            return [CityListPresenter new];
        });
        
        context(@"with data", ^{
            it(@"call db delete", ^{
                [presenter loadDataWithGateway:dbGateway];
                
                [[dbGateway should] receive:@selector(deleteModelWithPrimaryKey:finished:)];
                [presenter removeDataAtIndex:0 withDBGateway:dbGateway finished:^{}];
            });
        });
    });
});

SPEC_END
