//
//  APIForecastToEntityForecastTransformerSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "APIForecastToEntityForecastTransformer.h"
#import "MTLJSONAdapter.h"
#import "CityForecastResponseModel.h"
#import "CityForecastEntity.h"

SPEC_BEGIN(APIForecastToEntityForecastTransformerSpec)

describe(@"APIForecastToEntityForecastTransformer", ^{
    context(@"transform", ^{
        context(@"given model with current only", ^{
            let(json, ^{
                return @{
                         @"data" : @{ @"current_condition" :
                                          @[ @{ @"temp_C" : @"19", @"temp_F" : @"19",
                                                @"weatherIconUrl": @[ @{ @"value": @"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png" } ] } ],
                                      }};
            });
            
            let(responseModel, ^{
                NSError *error = nil;
                return [MTLJSONAdapter modelOfClass:[CityForecastResponseModel class] fromJSONDictionary:json error:&error];
            });
            
            it(@"convert successfuly", ^{
                id forecastEntity = [[APIForecastToEntityForecastTransformer new] apply:responseModel];
                [[forecastEntity shouldNot] beNil];
            });
        });
        
        context(@"given model with forecasts", ^{
            let(json, ^{
                return @{
                         @"data" : @{ @"current_condition" :
                                          @[ @{ @"temp_C" : @"19", @"temp_F" : @"19",
                                                @"weatherIconUrl": @[ @{ @"value": @"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png" } ] } ],
                                      @"weather": @[
                                              @{
                                                  @"date": @"2016-07-17",
                                                  @"maxtempC": @"23",
                                                  @"maxtempF": @"73",
                                                  @"mintempC": @"8",
                                                  @"mintempF": @"46",
                                                  },
                                              @{
                                                  @"date": @"2016-07-18",
                                                  @"maxtempC": @"20",
                                                  @"maxtempF": @"68",
                                                  @"mintempC": @"6",
                                                  @"mintempF": @"42",
                                                  }
                                              ]
                                      }};

            });
            
            let(responseModel, ^{
                NSError *error = nil;
                return [MTLJSONAdapter modelOfClass:[CityForecastResponseModel class] fromJSONDictionary:json error:&error];
            });
            
            it(@"convert successfuly", ^{
                CityForecastEntity *forecastEntity = [[APIForecastToEntityForecastTransformer new] apply:responseModel];
                [[forecastEntity shouldNot] beNil];
                [[forecastEntity.nextDaysForecast shouldNot] beNil];
                [[theValue(forecastEntity.nextDaysForecast.count) should] equal:theValue(1)];
            });
        });
        
    });
});

SPEC_END