//
//  SampleDBGateway.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "SampleDBGateway.h"

@implementation SampleDBGateway

- (NSArray *)loadWhere:(NSString *)query {
    NSObject *fake = [NSObject new];
    return @[fake];
}

- (NSArray *)loadAll {
    NSObject *fake = [NSObject new];
    return @[fake];
}

- (void)saveModel:(id)model {
    
}

- (void) deleteModelWithPrimaryKey:(NSString *)primaryKey finished:(void (^)(void))finished {
    
}

- (void) updateModel:(id)model finished:(void (^)(void))finished {
    
}

@end
