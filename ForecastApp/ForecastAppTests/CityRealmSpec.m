//
//  CityRealmSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "SearchCityModel.h"
#import "CityRealm.h"

SPEC_BEGIN(CityRealmSpec)

describe(@"CityRealm", ^{
    context(@"Initialize", ^{
        let(json, ^{
            return @{
                     @"areaName" : @[ @{ @"value" : @"Sao Paulo" } ],
                     @"country" : @[ @{ @"value" : @"Brazil" } ],
                     @"latitude" : @"51.517",
                     @"longitude" : @"-0.106",
                     @"population" : @"7421228",
                     @"region" : @[ @{ @"value" : @"Sao Paulo" } ],
                     @"weatherUrl" : @[ @{ @"value" : @"http://api.worldweatheronline.com/v2/weather.aspx?q=51.5171,-0.1062" } ]
                     } ;
        });
        
        let(cityModel, ^{
            NSError *error = nil;
            return [MTLJSONAdapter modelOfClass:SearchCityModel.class fromJSONDictionary:json error:&error];
        });
        
        let(cityRealm, ^{
            return [[CityRealm alloc] initWithAPIModel:cityModel];
        });
        
        context(@"with api model", ^{
            it(@"created successufuly", ^{
                [[cityRealm shouldNot] beNil];
            });
            
            it(@"has name filled", ^{
                [[cityRealm.name shouldNot] beNil];
                [[cityRealm.name should] equal:@"Sao Paulo"];
            });
            
            it(@"has pkID filled", ^{
                [[cityRealm.pkID shouldNot] beNil];
                [[cityRealm.pkID should] equal:@"SaoPauloSaoPauloBrazil"];
            });
        });
        
        context(@"with dictionary", ^{
            let(cityRealm, ^{
                NSDictionary *dictionary = @{ @"city": @"Sao Paulo", @"region":@"Sao Paulo", @"country":@"Brazil" };
                return [[CityRealm alloc] initWithDictionary:dictionary];
            });
            
            it(@"created successufuly", ^{
                [[cityRealm shouldNot] beNil];
            });
            
            it(@"has name filled", ^{
                [[cityRealm.name shouldNot] beNil];
                [[cityRealm.name should] equal:@"Sao Paulo"];
            });
            
            it(@"has pkID filled", ^{
                [[cityRealm.pkID shouldNot] beNil];
                [[cityRealm.pkID should] equal:@"SaoPauloSaoPauloBrazil"];
            });
        });
    });
});

SPEC_END
