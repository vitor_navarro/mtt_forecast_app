//
//  CityWeatherRequestModelSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CityWeatherRequestModel.h"

SPEC_BEGIN(CityWeatherRequestModelSpec)

describe(@"CityWeatherRequestModel", ^{
    context(@"Initialize", ^{
        context(@"with properties filled", ^{
            let(requestModel, ^{
                CityWeatherRequestModel *model = [CityWeatherRequestModel new];
                [model setQuery:@"CityName"];
                [model setDays:@"5"];
                return model;
            });
            
            it(@"converts to json with no error", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:requestModel error:&error];
                [[error should] beNil];
                [[JSONDictionary shouldNot] beNil];
            });
            
            it(@"JSON output has query as q", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:requestModel error:&error];
                [[[JSONDictionary allKeys] should] contain:@"q"];
            });
            
            it(@"JSON output has days as num_of_days", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:requestModel error:&error];
                [[[JSONDictionary allKeys] should] contain:@"num_of_days"];
            });
        });
    });
});

SPEC_END
