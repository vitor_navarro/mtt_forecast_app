//
//  SearchCityResponseModel.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "SearchCityResponseModel.h"
#import "MTLJSONAdapter.h"
#import "SearchCityModel.h"

SPEC_BEGIN(SearchCityResponseModelSpec)

describe(@"SearchCityResponseModel", ^{
    context(@"Initialize", ^{
        context(@"with json", ^{
            let(json, ^{
                return @{
                         @"search_api" : @{ @"result" : @[ @{ @"areaName" : @[ @{ @"value" : @"London" } ],
                @"country" : @[ @{ @"value" : @"United Kingdom" } ],
                @"latitude" : @"51.517",
                @"longitude" : @"-0.106",
                @"population" : @"7421228",
                @"region" : @[ @{ @"value" : @"City of London, Greater London" } ],
                @"weatherUrl" : @[ @{ @"value" : @"http://api.worldweatheronline.com/v2/weather.aspx?q=51.5171,-0.1062" } ]
                                                               } ] } };
            });
            
            it(@"converts from json with no error", ^{
                NSError *error = nil;
                SearchCityResponseModel *responseModel = [MTLJSONAdapter modelOfClass:SearchCityResponseModel.class fromJSONDictionary:json error:&error];
                [[error should] beNil];
                [[responseModel shouldNot] beNil];
            });
            
            it(@"model has at least one city", ^{
                NSError *error = nil;
                SearchCityResponseModel *responseModel = [MTLJSONAdapter modelOfClass:SearchCityResponseModel.class fromJSONDictionary:json error:&error];
                [[theValue([responseModel.cities count]) should] equal:theValue(1)];
            });
            
            it(@"city is not empty", ^{
                NSError *error = nil;
                SearchCityResponseModel *responseModel = [MTLJSONAdapter modelOfClass:SearchCityResponseModel.class fromJSONDictionary:json error:&error];
                SearchCityModel *firstItem = [responseModel.cities firstObject];
                [[firstItem shouldNot] beNil];
                [[firstItem.city shouldNot] beNil];
            });
        });
    });
});

SPEC_END
