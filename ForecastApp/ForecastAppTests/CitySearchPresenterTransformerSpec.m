//
//  CitySearchPresenterTransformerSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//


#import "Kiwi.h"
#import "CitySearchPresenterTransformer.h"
#import "MTLJSONAdapter.h"
#import "SearchCityModel.h"

SPEC_BEGIN(CitySearchPresenterTransformerSpec)

describe(@"CitySearchPresenterTransformer", ^{
    context(@"transform search response", ^{
        let(json, ^{
            return @{
                      @"areaName" : @[ @{ @"value" : @"London" } ],
                      @"country" : @[ @{ @"value" : @"United Kingdom" } ],
                                                          @"latitude" : @"51.517",
                                                          @"longitude" : @"-0.106",
                                                          @"population" : @"7421228",
                                                          @"region" : @[ @{ @"value" : @"City of London, Greater London" } ],
                                                          @"weatherUrl" : @[ @{ @"value" : @"http://api.worldweatheronline.com/v2/weather.aspx?q=51.5171,-0.1062" } ]
                                                          } ;
        });
        
        let(cityModel, ^{
            NSError *error = nil;
            return [MTLJSONAdapter modelOfClass:SearchCityModel.class fromJSONDictionary:json error:&error];
        });

        it(@"convert successfuly", ^{
            id tableCellModelArray = [[CitySearchPresenterTransformer new] apply:cityModel];
            [[tableCellModelArray shouldNot] beNil];
        });
    });
});

SPEC_END
