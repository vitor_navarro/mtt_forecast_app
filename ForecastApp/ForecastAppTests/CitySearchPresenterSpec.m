//
//  CitySearchPresenterSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CitySearchPresenter.h"
#import "Nocilla.h"
#import "CitySearchGateway.h"
#import "SampleDBGateway.h"

SPEC_BEGIN(CitySearchPresenterSpec)

describe(@"CitySearchPresenter", ^{
    context(@"On initialization", ^{
        let(presenter, ^{
            return [CitySearchPresenter new];
        });
        
        context(@"no data is present", ^{
            it(@"count is 0", ^{
                [[theValue([presenter dataCount]) should] equal:theValue(0)];
            });
            
            it(@"data should return new model", ^{
                [[[presenter dataAtIndex:0] shouldNot] beNil];
            });
        });
        
        context(@"after loading data", ^{
            let(json, ^{
                return @"{\"search_api\":{\"result\":[{\"areaName\":[{\"value\":\"London\"}],\"country\":[{\"value\":\"United Kingdom\"}],\"region\":[{\"value\":\"City of London, Greater London\"}],\"latitude\":\"51.517\",\"longitude\":\"-0.106\",\"population\":\"7421228\",\"weatherUrl\":[{\"value\":\"http://api.worldweatheronline.com/v2/weather.aspx?q=51.5171,-0.1062\"}]},{\"areaName\":[{\"value\":\"London\"}],\"country\":[{\"value\":\"Kiribati\"}],\"region\":[{\"value\":\"\"}],\"latitude\":\"1.983\",\"longitude\":\"-157.467\",\"population\":\"0\",\"weatherUrl\":[{\"value\":\"http://api.worldweatheronline.com/v2/weather.aspx?q=1.9833,-157.4667\"}]}]}}";
            });
            
            beforeAll(^{
                [[LSNocilla sharedInstance] start];
            });
            afterAll(^{
                [[LSNocilla sharedInstance] stop];
            });
            afterEach(^{
                [[LSNocilla sharedInstance] clearStubs];
            });
            
            it(@"return mock data successfuly", ^{
                stubRequest(@"GET", @"https://api.worldweatheronline.com/premium/v1/search.ashx?format=json&key=580f9005fa694bda942201539161407&num_of_results=50&q=London")
                .andReturn(200).
                withHeaders(@{@"Content-Type": @"application/json"}).
                withBody(json);
                
                [presenter searchGateway:[CitySearchGateway new] withQuery:@"London" finished:^(NSError *error) {}];
                
                [[expectFutureValue(theValue([presenter dataCount])) shouldEventually] equal:theValue(2)];
            });
        });
    });
    
    context(@"Manipulating data", ^{
        let(presenter, ^{
            return [CitySearchPresenter new];
        });
        
        let(dbGateway, ^id{
            return [SampleDBGateway new];
        });
        context(@"on saving", ^{
            it(@"call db gateway", ^{
                [presenter stub:@selector(hasData) andReturn:theValue(YES)];
                [presenter stub:@selector(originalDataAtIndex:) andReturn:[KWAny any]];
                
                [[dbGateway should] receive:@selector(saveModel:)];
                [presenter saveDataAtIndex:0 withDBGateway:dbGateway];
            });
        });
    });
});

SPEC_END
