//
//  CityForecastPresenterSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CityForecastPresenter.h"
#import "CityEntityDBGateway.h"
#import "SampleNetworkGateway.h"
#import "APIForecastToEntityForecastTransformer.h"

SPEC_BEGIN(CityForecastPresenterSpec)

describe(@"CityForecastPresenter", ^{
    
    let(dbGateway, ^id{
        return [CityEntityDBGateway new];
    });
    
    let(networkGateway, ^id{
        return [SampleNetworkGateway new];
    });
    
    context(@"On initialization", ^{
        let(presenter, ^{
            return [CityForecastPresenter new];
        });
        
        context(@"no data is present", ^{
            it(@"count is 0", ^{
                [[theValue([presenter dataCount]) should] equal:theValue(0)];
            });
            
            it(@"data should return new model", ^{
                [[[presenter dataAtIndex:0] shouldNot] beNil];
            });
        });
    });
    
    context(@"when loading data", ^{
        let(presenter, ^{
            CityForecastPresenter *forecastPresenter = [CityForecastPresenter new];
            [forecastPresenter setApiToEntityTransformer:[APIForecastToEntityForecastTransformer new]];
            return forecastPresenter;
        });
        
        it(@"call proper method from db gateway", ^{
            [[dbGateway should] receive:@selector(loadWhere:)];
            [presenter loadSelectedCity:dbGateway];
        });
        
        it(@"call proper method from net gateway", ^{
            [presenter loadSelectedCity:dbGateway];
            
            [[networkGateway should] receive:@selector(fetchSingleWithParams:success:failure:)];            
            [presenter loadDataWithNetGateway:networkGateway finished:^(NSError *error) {}];
        });
        
        it(@"count should be greater than 1", ^{
            [presenter loadSelectedCity:dbGateway];
            
            [presenter loadDataWithNetGateway:networkGateway finished:^(NSError *error) {}];
            
            [[theValue([presenter dataCount]) should] beGreaterThan:theValue(1)];
        });
    });
});

SPEC_END