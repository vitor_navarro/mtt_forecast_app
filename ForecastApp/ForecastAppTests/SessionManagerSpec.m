//
//  SessionManager.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "SessionManager.h"

SPEC_BEGIN(SessionManagerSpec)

describe(@"SessionManager", ^{
    context(@"Initialize", ^{
        context(@"with proper url", ^{
            let(managerInstance, ^{
                return [SessionManager shared:@"http://sampleurl.com"];
            });
            
            it(@"has url configured", ^{
                NSURL *baseUrl = [managerInstance baseURL];
                [[baseUrl shouldNot] beNil];
            });
        });        
    });
});

SPEC_END
