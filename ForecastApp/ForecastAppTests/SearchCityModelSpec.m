//
//  SearchCityModelSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "SearchCityModel.h"

SPEC_BEGIN(SearchCityModelSpec)

describe(@"SearchCityModel", ^{
    context(@"Initialize", ^{
        context(@"with city name filled", ^{
            let(searchCityModel, ^{
                SearchCityModel *model = [SearchCityModel new];
                [model setCity:@"CityName"];
                return model;
            });
            
            it(@"converts to json with no error", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:searchCityModel error:&error];
                [[error should] beNil];
                [[JSONDictionary shouldNot] beNil];
            });
            
            it(@"JSON output has city as areaName", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:searchCityModel error:&error];
                [[[JSONDictionary allKeys] should] contain:@"areaName"];
            });
        });
    });
});

SPEC_END