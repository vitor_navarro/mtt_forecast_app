//
//  SearchCityRequestModelSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "SearchCityRequestModel.h"

SPEC_BEGIN(SearchCityRequestModelSpec)

describe(@"SearchCityRequestModel", ^{
    context(@"Initialize", ^{
        context(@"with properties filled", ^{
            let(requestModel, ^{
                SearchCityRequestModel *model = [SearchCityRequestModel new];
                [model setQuery:@"CityName"];
                [model setMaxResults:@"50"];
                return model;
            });
            
            it(@"converts to json with no error", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:requestModel error:&error];
                [[error should] beNil];
                [[JSONDictionary shouldNot] beNil];
            });
            
            it(@"JSON output has query as q", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:requestModel error:&error];
                [[[JSONDictionary allKeys] should] contain:@"q"];
            });
            
            it(@"JSON output has maxResults as num_of_results", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:requestModel error:&error];
                [[[JSONDictionary allKeys] should] contain:@"num_of_results"];
            });
        });
    });
});

SPEC_END