//
//  CityForecastGatewaySpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CityWeatherRequestModel.h"
#import "CityForecastGateway.h"
#import "CityForecastResponseModel.h"
#import "Nocilla.h"

SPEC_BEGIN(CityForecastGatewaySpec)

describe(@"CityForecastGateway", ^{
    context(@"On search", ^{
        let(json, ^{
            return @"{ \"data\" : { \"current_condition\" : [ { \"FeelsLikeC\" : \"19\",\r\n            \"FeelsLikeF\" : \"66\",\r\n            \"cloudcover\" : \"0\",\r\n            \"humidity\" : \"24\",\r\n            \"observation_time\" : \"07:23 PM\",\r\n            \"precipMM\" : \"0.0\",\r\n            \"pressure\" : \"1018\",\r\n            \"temp_C\" : \"19\",\r\n            \"temp_F\" : \"66\",\r\n            \"visibility\" : \"10\",\r\n            \"weatherCode\" : \"113\",\r\n            \"weatherDesc\" : [ { \"value\" : \"Sunny\" } ],\r\n            \"weatherIconUrl\" : [ { \"value\" : \"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png\" } ],\r\n            \"winddir16Point\" : \"W\",\r\n            \"winddirDegree\" : \"280\",\r\n            \"windspeedKmph\" : \"19\",\r\n            \"windspeedMiles\" : \"12\"\r\n          } ],\r\n      \"request\" : [ { \"query\" : \"Sao Paulo, Brazil\",\r\n            \"type\" : \"City\"\r\n          } ],\r\n      \"weather\" : [ { \"astronomy\" : [ { \"moonrise\" : \"03:48 PM\",\r\n                  \"moonset\" : \"04:30 AM\",\r\n                  \"sunrise\" : \"06:47 AM\",\r\n                  \"sunset\" : \"05:38 PM\"\r\n                } ],\r\n            \"date\" : \"2016-07-17\",\r\n            \"hourly\" : [ { \"DewPointC\" : \"14\",\r\n                  \"DewPointF\" : \"57\",\r\n                  \"FeelsLikeC\" : \"19\",\r\n                  \"FeelsLikeF\" : \"65\",\r\n                  \"HeatIndexC\" : \"19\",\r\n                  \"HeatIndexF\" : \"65\",\r\n                  \"WindChillC\" : \"19\",\r\n                  \"WindChillF\" : \"65\",\r\n                  \"WindGustKmph\" : \"4\",\r\n                  \"WindGustMiles\" : \"2\",\r\n                  \"chanceoffog\" : \"0\",\r\n                  \"chanceoffrost\" : \"0\",\r\n                  \"chanceofhightemp\" : \"0\",\r\n                  \"chanceofovercast\" : \"0\",\r\n                  \"chanceofrain\" : \"0\",\r\n                  \"chanceofremdry\" : \"0\",\r\n                  \"chanceofsnow\" : \"0\",\r\n                  \"chanceofsunshine\" : \"100\",\r\n                  \"chanceofthunder\" : \"0\",\r\n                  \"chanceofwindy\" : \"0\",\r\n                  \"cloudcover\" : \"0\",\r\n                  \"humidity\" : \"74\",\r\n                  \"precipMM\" : \"0.0\",\r\n                  \"pressure\" : \"1020\",\r\n                  \"tempC\" : \"23\",\r\n                  \"tempF\" : \"73\",\r\n                  \"time\" : \"24\",\r\n                  \"visibility\" : \"10\",\r\n                  \"weatherCode\" : \"113\",\r\n                  \"weatherDesc\" : [ { \"value\" : \"Sunny\" } ],\r\n                  \"weatherIconUrl\" : [ { \"value\" : \"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png\" } ],\r\n                  \"winddir16Point\" : \"WSW\",\r\n                  \"winddirDegree\" : \"256\",\r\n                  \"windspeedKmph\" : \"10\",\r\n                  \"windspeedMiles\" : \"6\"\r\n                } ],\r\n            \"maxtempC\" : \"23\",\r\n            \"maxtempF\" : \"73\",\r\n            \"mintempC\" : \"8\",\r\n            \"mintempF\" : \"46\",\r\n            \"uvIndex\" : \"6\"\r\n          },\r\n          { \"astronomy\" : [ { \"moonrise\" : \"04:39 PM\",\r\n                  \"moonset\" : \"05:22 AM\",\r\n                  \"sunrise\" : \"06:47 AM\",\r\n                  \"sunset\" : \"05:39 PM\"\r\n                } ],\r\n            \"date\" : \"2016-07-18\",\r\n            \"hourly\" : [ { \"DewPointC\" : \"9\",\r\n                  \"DewPointF\" : \"48\",\r\n                  \"FeelsLikeC\" : \"11\",\r\n                  \"FeelsLikeF\" : \"52\",\r\n                  \"HeatIndexC\" : \"12\",\r\n                  \"HeatIndexF\" : \"53\",\r\n                  \"WindChillC\" : \"11\",\r\n                  \"WindChillF\" : \"52\",\r\n                  \"WindGustKmph\" : \"11\",\r\n                  \"WindGustMiles\" : \"7\",\r\n                  \"chanceoffog\" : \"0\",\r\n                  \"chanceoffrost\" : \"0\",\r\n                  \"chanceofhightemp\" : \"0\",\r\n                  \"chanceofovercast\" : \"0\",\r\n                  \"chanceofrain\" : \"0\",\r\n                  \"chanceofremdry\" : \"0\",\r\n                  \"chanceofsnow\" : \"0\",\r\n                  \"chanceofsunshine\" : \"100\",\r\n                  \"chanceofthunder\" : \"0\",\r\n                  \"chanceofwindy\" : \"0\",\r\n                  \"cloudcover\" : \"49\",\r\n                  \"humidity\" : \"82\",\r\n                  \"precipMM\" : \"0.0\",\r\n                  \"pressure\" : \"1024\",\r\n                  \"tempC\" : \"20\",\r\n                  \"tempF\" : \"68\",\r\n                  \"time\" : \"24\",\r\n                  \"visibility\" : \"10\",\r\n                  \"weatherCode\" : \"116\",\r\n                  \"weatherDesc\" : [ { \"value\" : \"Partly Cloudy \" } ],\r\n                  \"weatherIconUrl\" : [ { \"value\" : \"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png\" } ],\r\n                  \"winddir16Point\" : \"E\",\r\n                  \"winddirDegree\" : \"99\",\r\n                  \"windspeedKmph\" : \"12\",\r\n                  \"windspeedMiles\" : \"7\"\r\n                } ],\r\n            \"maxtempC\" : \"20\",\r\n            \"maxtempF\" : \"68\",\r\n            \"mintempC\" : \"6\",\r\n            \"mintempF\" : \"42\",\r\n            \"uvIndex\" : \"6\"\r\n          }\r\n        ]\r\n    } }";
        });
        
        context(@"with mock api", ^{
            beforeAll(^{
                [[LSNocilla sharedInstance] start];
            });
            afterAll(^{
                [[LSNocilla sharedInstance] stop];
            });
            afterEach(^{
                [[LSNocilla sharedInstance] clearStubs];
            });
            
            it(@"convert successfuly", ^{
                stubRequest(@"GET", @"https://api.worldweatheronline.com/premium/v1/weather.ashx?format=json&fx24=no&key=580f9005fa694bda942201539161407&mca=no&num_of_days=6&q=sao%20paulo&tp=24").
                andReturn(200).
                withHeaders(@{@"Content-Type": @"application/json"}).
                withBody(json);
                
                __block id data;
                
                CityForecastGateway *gateway = [[CityForecastGateway alloc] init];
                
                [gateway fetchSingleWithParams:@{@"query":@"sao paulo"} success:^(id _data) {
                    data = _data;
                } failure:^(NSError *error) {}];
                 
                [[expectFutureValue(data) shouldEventually] beNonNil];
            });
        });
    });
});

SPEC_END
