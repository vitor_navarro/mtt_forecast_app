//
//  CityForecastModelSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CityForecastModel.h"

SPEC_BEGIN(CityForecastModelSpec)

describe(@"CityForecastModel", ^{
    context(@"Initialize", ^{
        context(@"with properties filled", ^{
            let(model, ^{
                CityForecastModel *cityForecast = [CityForecastModel new];
                [cityForecast setDate:@"2016-07-17"];
                [cityForecast setMinTemperature:@"19"];
                [cityForecast setMaxTemperature:@"24"];
                [cityForecast setMinTemperatureFahrenheit:@"50"];
                [cityForecast setMaxTemperatureFahrenheit:@"66"];
                return cityForecast;
            });
            
            it(@"converts to json with no error", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:model error:&error];
                [[error should] beNil];
                [[JSONDictionary shouldNot] beNil];
            });
            
            it(@"JSON output has maxTemperature as maxtempC", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:model error:&error];
                [[[JSONDictionary allKeys] should] contain:@"maxtempC"];
            });
        });
    });
});

SPEC_END