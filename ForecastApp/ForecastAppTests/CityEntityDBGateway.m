//
//  CityEntityDBGateway.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "CityEntityDBGateway.h"
#import "CityEntity.h"

@implementation CityEntityDBGateway

- (NSArray *)loadWhere:(NSString *)query {
    return @[[[CityEntity alloc] initWithName:@"name"
                                                       region:@"reg"
                                                      country:@"country"
                                                     uniqueID:@"id"]];
}

- (NSArray *)loadAll {
    return @[[[CityEntity alloc] initWithName:@"name"
                                       region:@"reg"
                                      country:@"country"
                                     uniqueID:@"id"]];
}

@end
