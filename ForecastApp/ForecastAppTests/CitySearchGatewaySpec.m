//
//  CitySearchGatewaySpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/16/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CitySearchGateway.h"
#import "SearchCityResponseModel.h"
#import "Nocilla.h"

SPEC_BEGIN(CitySearchGatewaySpec)

describe(@"CitySearchGateway", ^{
    context(@"On search", ^{
        let(json, ^{
            return @"{\"search_api\":{\"result\":[{\"areaName\":[{\"value\":\"London\"}],\"country\":[{\"value\":\"United Kingdom\"}],\"region\":[{\"value\":\"City of London, Greater London\"}],\"latitude\":\"51.517\",\"longitude\":\"-0.106\",\"population\":\"7421228\",\"weatherUrl\":[{\"value\":\"http://api.worldweatheronline.com/v2/weather.aspx?q=51.5171,-0.1062\"}]},{\"areaName\":[{\"value\":\"London\"}],\"country\":[{\"value\":\"Kiribati\"}],\"region\":[{\"value\":\"\"}],\"latitude\":\"1.983\",\"longitude\":\"-157.467\",\"population\":\"0\",\"weatherUrl\":[{\"value\":\"http://api.worldweatheronline.com/v2/weather.aspx?q=1.9833,-157.4667\"}]}]}}";
        });
        
        context(@"with mock api", ^{
            beforeAll(^{
                [[LSNocilla sharedInstance] start];
            });
            afterAll(^{
                [[LSNocilla sharedInstance] stop];
            });
            afterEach(^{
                [[LSNocilla sharedInstance] clearStubs];
            });
            
            it(@"convert successfuly", ^{
                stubRequest(@"GET", @"https://api.worldweatheronline.com/premium/v1/search.ashx?format=json&key=580f9005fa694bda942201539161407&num_of_results=50&q=London")
                .andReturn(200).
                withHeaders(@{@"Content-Type": @"application/json"}).                
                withBody(json);
                
                __block NSArray *data;
                
                CitySearchGateway *gateway = [[CitySearchGateway alloc] init];
                
                [gateway search:@"London" finished:^(NSArray *_data) {
                    data = _data;
                } failed:^(NSError *error) {}];
                
                [[expectFutureValue(data) shouldEventually] haveCountOf:2];
            });
        });
    });
});

SPEC_END