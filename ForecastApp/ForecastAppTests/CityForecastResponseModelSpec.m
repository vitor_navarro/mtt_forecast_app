//
//  CityForecastResponseModelSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CityForecastResponseModel.h"
#import "MTLJSONAdapter.h"
#import "CityForecastModel.h"
#import "CityCurrentWeatherModel.h"

SPEC_BEGIN(CityForecastResponseModelSpec)

describe(@"CityForecastResponseModel", ^{
    context(@"Initialize", ^{
        context(@"with json", ^{
            let(json, ^{
                return @{
                         @"data" : @{ @"current_condition" :
                                          @[ @{ @"temp_C" : @"19", @"temp_F" : @"19",
                                                @"weatherIconUrl": @[ @{ @"value": @"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png" } ] } ],
                                      @"weather": @[
                                            @{
                                              @"date": @"2016-07-17",
                                              @"maxtempC": @"23",
                                              @"maxtempF": @"73",
                                              @"mintempC": @"8",
                                              @"mintempF": @"46",
                                              },
                                            @{
                                            @"date": @"2016-07-18",
                                            @"maxtempC": @"20",
                                            @"maxtempF": @"68",
                                            @"mintempC": @"6",
                                            @"mintempF": @"42",
                                            }
                                            ]
                        }};
            });
            
            it(@"converts from json with no error", ^{
                NSError *error = nil;
                CityForecastResponseModel *responseModel = [MTLJSONAdapter modelOfClass:CityForecastResponseModel.class fromJSONDictionary:json error:&error];
                [[error should] beNil];
                [[responseModel shouldNot] beNil];
            });
            
            it(@"model has at least two weather forecast", ^{
                NSError *error = nil;
                CityForecastResponseModel *responseModel = [MTLJSONAdapter modelOfClass:CityForecastResponseModel.class fromJSONDictionary:json error:&error];
                [[theValue([responseModel.forecasts count]) should] equal:theValue(2)];
            });
            
            it(@"current weather is not empty", ^{
                NSError *error = nil;
                CityForecastResponseModel *responseModel = [MTLJSONAdapter modelOfClass:CityForecastResponseModel.class fromJSONDictionary:json error:&error];
                [[responseModel.current shouldNot] beNil];
            });
        });
    });
});

SPEC_END
