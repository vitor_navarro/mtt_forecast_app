//
//  CityCurrentWeatherModelSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CityCurrentWeatherModel.h"

SPEC_BEGIN(CityCurrentWeatherModelSpec)

describe(@"CityCurrentWeatherModel", ^{
    context(@"Initialize", ^{
        context(@"with properties filled", ^{
            let(model, ^{
                CityCurrentWeatherModel *cityWeather = [CityCurrentWeatherModel new];
                [cityWeather setTemperature:@"19"];
                [cityWeather setTemperatureInFahrenheit:@"66"];
                [cityWeather setWeatherIconUrl:@"http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png"];
                return cityWeather;
            });
            
            it(@"converts to json with no error", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:model error:&error];
                [[error should] beNil];
                [[JSONDictionary shouldNot] beNil];
            });
            
            it(@"JSON output has temperature as temp_C", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:model error:&error];
                [[[JSONDictionary allKeys] should] contain:@"temp_C"];
            });
            
            it(@"JSON output has fahrenheit as temp_F", ^{
                NSError *error = nil;
                NSDictionary *JSONDictionary = [MTLJSONAdapter JSONDictionaryFromModel:model error:&error];
                [[[JSONDictionary allKeys] should] contain:@"temp_F"];
            });
        });
    });
});

SPEC_END
