//
//  CityListPresenterTransformerSpec.m
//  ForecastApp
//
//  Created by Vitor Navarro on 7/17/16.
//  Copyright © 2016 Vitor Navarro. All rights reserved.
//

#import "Kiwi.h"
#import "CityListPresenterTransformer.h"
#import "CityRealm.h"

SPEC_BEGIN(CityListPresenterTransformerSpec)

describe(@"CityListPresenterTransformer", ^{
    context(@"transform search response", ^{
        
        let(cityModel, ^{
            NSDictionary *dictionary = @{ @"city": @"Sao Paulo", @"region":@"Sao Paulo", @"country":@"Brazil" };
            return [[CityRealm alloc] initWithDictionary:dictionary];
        });
        
        it(@"convert successfuly", ^{
            id tableCellModelArray = [[CityListPresenterTransformer new] apply:cityModel];
            [[tableCellModelArray shouldNot] beNil];
        });
    });
});

SPEC_END